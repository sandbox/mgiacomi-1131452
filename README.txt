<pre>
README.txt
==========

Voom is framework wrapped in a Drupal module. Voom is implemented in a way that allows you to interfaces
with core drupal functionality in a way that will be more natural to developers that are coming from Rails
or Java SpringMVC.

If you are looking to build an extension to drupal core, or looking to provide extra functionality on top of
an existing module, then don't bother with Voom. If you want to do custom application development, something
you would generally consider using CakePHP or Rails for, than Voom is what you are looking for.

Here are some of Voom's features:

    MVC: Voom allows you to create applications inside of Drupal using the Model-View-Controller paradigm.
    This encourages one to develop in a way that provides a high degree of separation between your models,
    controllers, and views. For more details see "Model view controller" on wikipedia. Unlike generic MVC
    frameworks Voom has many Controller and Model features that are Drupal specific. Allowing you to achieve
    a higher level of Drupal integration.

    OOP: Voom allows you to code in drupal using OOP. Your application will end up being wrapped in a module
    that Voom will bootstrap.

    Dependency Injection: Voom acts as a small DI engine for Controller, Services, and DAO's in your module.
    This allows you to write code that is cleaner and can be more easily unit tested.

    Annotation Support: Voom allows you to use annotations to do add declaratively add functionality to your
    application. We currently support annotations for dependency injection, menu support, breadcrumbs,
    transactions, and security.

    Global Transactions: Voom abstracts Drupal's transactional support by allowing you annotate methods as
    transactional. This allows you to create transactional boundaries in services that may span may DAO. If
    an exception is thrown by the annotated method, then the transaction is rolled back otherwise it is
    committed. Transactional methods are commit or rollback all databases together. So if an annotated services
    method makes calls to DAO's using two different database, than a exception will rollback changes to both
    databases.
    * This is not XA or 2 phase commit.

SAMPLE CODE
===========
You can find sample and documentation at http://www.gltech.com/voom

COMPATIBILITY NOTES
===================
Currently this module is only compatible with Drupal 7.x.

AUTHOR/MAINTAINER
=================
-Matt Giacomini <mgiacomi@gltech.com>
http://www.gltech.com
</pre>