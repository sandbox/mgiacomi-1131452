<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

define('APP_PATH', drupal_get_path('module', 'voom'));

global $voom_block_menu;
global $voom_context;

function voom_help($section) {
  switch ($section) {
    case 'admin/help#voom':
      return file_get_contents( dirname(__FILE__) . "/README.txt");
  }
}

// Setup hook to allow modules to only use ONM
function voom_init_onm($nodeDir) {
    require_once 'lib/Doctrine/Common/ClassLoader.php';
    $doctrineLoader = new \Doctrine\Common\ClassLoader('Doctrine\Common', APP_PATH . '/lib');
    $doctrineLoader->register();

    $gltechLoader = new \Doctrine\Common\ClassLoader('Gltech', APP_PATH . '/lib');
    $gltechLoader->register();

    $nodeLoader = new \Doctrine\Common\ClassLoader(null, $nodeDir);
    $nodeLoader->register();
}

function voom_block_info() {
    $blocks[0]['info'] = t('Voom Menu');
    $blocks[0]['cache'] = DRUPAL_NO_CACHE;
    return $blocks;
}

function voom_block_view($delta = 0) {
    global $voom_block_menu;
    if(isset($voom_block_menu->data)) {
        $block['subject'] = $voom_block_menu->title;
        $block['content'] = menu_tree_output($voom_block_menu->data);
        return $block;
    }
}

function voom_preprocess_page(&$variables) {
    global $voom_context;
    if(isset($voom_context)) {
        $variables['attributes_array']['voom_module'] = $voom_context->module;
        $variables['attributes_array']['voom_controller'] = $voom_context->controller;
        $variables['attributes_array']['voom_action'] = $voom_context->action;
        $variables['attributes_array']['voom_id'] = $voom_context->id;
    }
}

function voom_preprocess(&$variables, $hook) {
    $modules = module_implements('voom_init');
    foreach($modules as $module) {
        if(substr($hook, 0, strlen($module)) == $module) {
            $variables['theme_hook_suggestions'][] = $hook;
        }
    }
}

function voom_theme() {
    $templates = array();

    $modules = module_implements('voom_init');
    foreach($modules as $module) {
        $moduleRoot = drupal_get_path('module', $module);
        $viewDirs = $moduleRoot . "/views";
        if($dirHandle = opendir($viewDirs)) {
            while(false !== ($directory = readdir($dirHandle))) {
                if(substr($directory,0,1) != ".") {
                    if(is_dir($viewDirs.'/'.$directory)) {
                        if($fileHandle = opendir($viewDirs.'/'.$directory)) {
                            while(false !== ($file = readdir($fileHandle))) {
                                if(substr($file,-8) == ".tpl.php") {
                                    $hook = $module .'__'. $directory .'__'. substr($file, 0, strlen($file)-8);
                                    $templates[$hook] = array(
                                        'path' => $viewDirs.'/'.$directory,
                                        'template' => substr($file, 0, strlen($file)-8)
                                    );
                                }
                            }
                            closedir($fileHandle);
                        }
                    }
                }
            }
            closedir($dirHandle);
        }
    }

    return $templates;

}

function voom_menu() {
    $items = array();

    $modules = module_implements('voom_init');
    foreach($modules as $module) {
        $modconfigs = module_invoke($module, 'voom_init');
        foreach($modconfigs as $modconfig) {
            $security = array();
            if(isset($modconfig['security'])) {
                $security[] = $modconfig['security'];
            }

            $items[$modconfig['context']] = array(
                'page callback' => 'mvc_dispatch',
                'page arguments' => array($module, $modconfig, 0, 1, 2),
                'title' => $modconfig['title'],
                'description' => $modconfig['description'],
                'menu_name' => $modconfig['menu_name'],
                'access callback' => 'menu_access',
                'access arguments' => $security,
                'type' => MENU_NORMAL_ITEM
            );
            $items[$modconfig['context'] . '/%'] = array(
               'page callback' => 'mvc_dispatch',
               'page arguments' => array($module, $modconfig, 0, 1, 2),
               'access callback' => TRUE,
               'type' => MENU_CALLBACK
            );
        }
    }

    return $items;
}

function menu_access($security = null) {
    if($security == null) {
        return true;
    }
    require_once 'lib/Doctrine/Common/ClassLoader.php';
    $doctrineLoader = new \Doctrine\Common\ClassLoader('Doctrine\Common', APP_PATH . '/lib');
    $doctrineLoader->register();
    $gltechLoader = new \Doctrine\Common\ClassLoader('Gltech', APP_PATH . '/lib');
    $gltechLoader->register();
    $parser = new \Gltech\Mvc\NestedValueParser();
    $securityMgr = new \Gltech\Mvc\SecurityMgr();
    $secured = new Gltech\Annotations\Secured($parser->parse($security));
    return $securityMgr->isAuthorized($secured);
}


function mvc_dispatch($module, $modconfig, $prefix, $controller, $action) {
    // Very lame work around for drupal bug http://drupal.org/node/520106#comment-2040054
    $item = menu_get_item();
    $item['href'] = $prefix;
    menu_set_item(NULL, $item);

    global $voom_block_menu;
    global $voom_context;

    require_once 'lib/Doctrine/Common/ClassLoader.php';
    $doctrineLoader = new \Doctrine\Common\ClassLoader('Doctrine\Common', APP_PATH . '/lib');
    $doctrineLoader->register();
    $gltechLoader = new \Doctrine\Common\ClassLoader('Gltech', APP_PATH . '/lib');
    $gltechLoader->register();

    $voom_context = \Gltech\Context::getInstance();
    $securityMgr = new \Gltech\Mvc\SecurityMgr();

    if($securityMgr->isModuleAuthorized($modconfig)) {
        if(isset($modconfig['context_as_controller']) && $modconfig['context_as_controller'] == true) {
            if(isset($controller)) {
                $action = $controller;
            }
            $controller = $modconfig['context'];
            $voom_context->context_as_controller = true;
        }

        if(empty($controller)) {
            $controller = $modconfig['default_controller'];
        }

        if(empty($action)) {
            if(isset($modconfig['default_action'])) {
                $action = $modconfig['default_action'];
            }
        }

        try {
            \Gltech\Context::getInstance()->modconfig = $modconfig;
            $masterController = new \Gltech\Mvc\Controller();
            $results = $masterController->doAction($module, $prefix, $controller, $action);

            $voom_block_menu = new \Gltech\Mvc\Menu();
            $renderer = new \Gltech\Mvc\Renderer();

            return $renderer->output($voom_block_menu, $results);
        }
        catch (\Gltech\Mvc\Exceptions\PageNotFound $e) {
            drupal_not_found();
        }
        catch (\Gltech\Mvc\Exceptions\AccessDenied $e) {
            drupal_access_denied();
        }
        catch (\Gltech\Mvc\Exceptions\ConfigException $e) {
            // some kind of logging here
        }
    }
    else {
        drupal_access_denied();
    }

}

function auth() {
    return new \Gltech\Mvc\AuthHelper();
}

function mvc_view($model = null, $view = null) {
    return new \Gltech\Mvc\View($view, $model);
}

function mvc_json($model, $raw = false) {
    return new \Gltech\Mvc\Json($model, $raw);
}

function mvc_redirect($controller, $action = null, $id = null) {
    return new \Gltech\Mvc\Redirect($controller, $action, $id);
}

?>