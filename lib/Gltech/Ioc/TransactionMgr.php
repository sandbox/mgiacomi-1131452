<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Ioc;

class TransactionMgr {
    private static $m_pInstance;
    private $transactions;
    private $methodId;
    private $methodSequence;

    private function __construct() {
        $this->transactions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->methodSequence = 0;
    }

    public static function getInstance() {
        if (!self::$m_pInstance) {
            self::$m_pInstance = new TransactionMgr();
        }

        return self::$m_pInstance;
    }

    public function generateMethodId() {
        return $this->methodSequence++;
    }

    public function startTransaction($methodId) {
        if(empty($this->methodId)) {
            $this->methodId = $methodId;

            global $databases;
            foreach(array_keys($databases) as $dbkey) {
                $this->transactions->set($dbkey, \Database::getConnection('default', $dbkey)->startTransaction());
            }
        }
    }

    public function endTransaction($methodId) {
        if($this->methodId == $methodId) {
            $this->methodId = null;
            $this->transactions->clear();
        }
    }

    public function rollbackTransaction($methodId) {
debug("attempted throw: ". $methodId);
        if($this->methodId == $methodId) {
            $this->methodId = null;

            foreach ($this->transactions->getKeys() as $dbkey) {
                $this->transactions->get($dbkey)->rollback();
            }
debug("rollback: ". $methodId);
        }
    }
}
