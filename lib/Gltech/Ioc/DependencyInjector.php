<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Ioc;
 
class DependencyInjector {
    private $annoReader;
    private $context;
    private $transactionMgr;

    public function __construct() {
        $this->annoReader = \Gltech\Annotations\AnnoReader::getInstance();
        $this->context = \Gltech\Context::getInstance();
        $this->transactionMgr = \Gltech\Ioc\TransactionMgr::getInstance();
    }

    public function injectDependencies($targetClass) {
        $properties = $this->annoReader->getInjectProperties($targetClass);

        foreach($properties as $property => $typeValue) {
            if($typeValue['type'] == 'database') {
                $this->injectDatabase($targetClass, $property, $typeValue['value']);
            }
            else {
                $this->injectClass($targetClass, $property, $typeValue['value']);
            }
        }
    }

    private function injectDatabase($targetClass, $property, $dbkey) {
        $targetClass->$property = \Database::getConnection('default', $dbkey);
    }

    private function injectClass($targetClass, $property, $fullClassName) {
        if(empty($fullClassName)) {
            $files = file_scan_directory($this->context->mvcRoot .'/services', '/^'. $property .'.php$/i');
            if(count($files) > 1) {
                throw new DependencyException("More then one class found for {". $property ."}");
            }
            if(count($files) < 1) {
                throw new DependencyException("Could not find class for reference {". $property ."} defined in class {". get_class($targetClass) ."}");
            }

            $classPath = key($files);
            $className = $files[key($files)]->name;
        }
        else {
            $classNameParts = explode("/", $fullClassName);
            $className = $classNameParts[count($classNameParts)-1];

            $subdir = "";
            if(count($classNameParts) > 1) {
                for($i = 0; $i < (count($classNameParts)-1); $i++) {
                    $subdir = $subdir .'/'. $classNameParts[$i];
                }
            }

            $classPath = $this->context->mvcRoot .'/services'. $subdir .'/'. $className .'.php';
        }

        if(file_exists($classPath)) {
            require_once $classPath;
            $class = new $className();

            $this->injectDependencies($class);
            $proxyClass = $this->generateProxy($class);
            $targetClass->$property = $proxyClass;
        }
    }

    public function generateProxy($class) {
        $reflect = new \ReflectionClass($class);
        $methods = $reflect->getMethods();
        $methodsStr = "";

        foreach($methods as $method) {
            $parameterStr = null;
            $parameterStrNoOption = null;
            if($method->isPublic() && substr($method->name, 0, 2) != '__') {
                foreach($method->getParameters() as $parameter) {
                    if(isset($parameterStr)) {
                        $parameterStr .= ",";
                    }
                    if(isset($parameterStrNoOption)) {
                        $parameterStrNoOption .= ",";
                    }

                    $parameterStr .= "$". $parameter->name;
                    $parameterStrNoOption .= "$". $parameter->name;

                    if($parameter->isOptional()) {
                        try {
                            if(is_array($parameter->getDefaultValue())) {
                                $parameterStr .= "=array()";
                            }
                            else if(is_string($parameter->getDefaultValue())) {
                                $parameterStr .= "='". $parameter->getDefaultValue() ."'";
                            }
                            else if(is_bool($parameter->getDefaultValue())) {
                                if($parameter->getDefaultValue()) {
                                    $parameterStr .= "=true";
                                }
                                else {
                                    $parameterStr .= "=false";
                                }
                            }
                            else if(is_null($parameter->getDefaultValue())) {
                                $parameterStr .= "=null";
                            }
                            else {
                                $parameterStr .= "=". $parameter->getDefaultValue();
                            }
                        }
                        catch (\ReflectionException $e) {}
                    }
                }
                $methodsStr .= "public function $method->name($parameterStr) {";
                $methodId = $this->transactionMgr->generateMethodId();

                if($this->annoReader->isTranactional($method)) {
                    $methodsStr .= "\$this->startTransaction($methodId);";
                    $methodsStr .= "try {";
                }

                $methodsStr .= "\$return_val = \$this->targetClass->$method->name($parameterStrNoOption);";

                if($this->annoReader->isTranactional($method)) {
                    $methodsStr .= "} catch (\\Exception \$e) {";
                    $methodsStr .= "\$this->rollbackTransaction($methodId);";
                    $methodsStr .= "throw \$e; }";
                    $methodsStr .= "\$this->endTransaction($methodId);";
                }

                $methodsStr .= "return \$return_val;";
                $methodsStr .= "}\n";
            }
        }

        $proxyName = $reflect->name . "Proxy";

        $classStr = "namespace Gltech\Ioc;";
        $classStr .= "class $proxyName extends AbstractProxy {";
        $classStr .= $methodsStr;
        $classStr .= "};";

        eval($classStr);
        $proxyNameWithNameSpace = "\\Gltech\\Ioc\\$proxyName";
        $proxyClass = new $proxyNameWithNameSpace();
        $proxyClass->setTargetClass($class);
        return $proxyClass;
    }
}