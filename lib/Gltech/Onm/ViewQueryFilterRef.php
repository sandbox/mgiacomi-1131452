<?php
    /*
    * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    *
    * This software consists of voluntary contributions made by many individuals
    * and is licensed under the LGPL.
    *
    * Author: Matt Giacomini <http://www.gltech.com>
    */

namespace Gltech\Onm;

class ViewQueryFilterRef extends ViewQueryFilterList {
    public function contains($ref) {
        $values = array();

        if(!is_array($ref)) {
            if(is_numeric($ref)) {
                $values = array($ref);
            }
            else {
                $values = array($ref->nid);
            }
        }
        else {
            foreach($ref as $value) {
                if(is_numeric($value)) {
                    $values = array($value);
                }
                else {
                    $values = array($value->nid);
                }
            }
        }

        $this->buildHandler($values);
        return $this->query;
    }

    public function does_not_contain($value) {
        if(!is_array($value)) {
            $value = array($value);
        }
        $this->buildHandler($value, 'not in');
        return $this->query;
    }
}
?>
