<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Onm;

class ViewQuery {
    private $view;
    private $handler;
    private $nodeType;
    private $fields;
    private $relationshipFields;
    private $fetchCount = 0;
    private $relationshipField;
    private $relationshipClass;

    public function __construct($node, $loadFields = true) {
        $this->nodeType = get_class($node);

        $annoReader = \Gltech\Annotations\AnnoReader::getInstance();
        $this->fields = $annoReader->getFields($node);

        // Generate View
        $this->view = new \view;
        $this->view->name = '';
        $this->view->description = '';
        $this->view->tag = 'default';
        $this->view->base_table = 'node';
        $this->view->human_name = '';
        $this->view->core = 7;
        $this->view->api_version = '3.0-alpha1';
        $this->view->disabled = FALSE;

        // Display: Master
        $this->handler = $this->view->new_display('default', 'Master', 'default');
        $this->handler->display->display_options['access']['type'] = 'perm';
        $this->handler->display->display_options['cache']['type'] = 'none';
        $this->handler->display->display_options['query']['type'] = 'views_query';
        $this->handler->display->display_options['query']['options']['query_comment'] = FALSE;
        $this->handler->display->display_options['pager']['type'] = 'none';
        $this->handler->display->display_options['pager']['options']['offset'] = '0';

        // Field: Content: Title
        $this->handler->display->display_options['fields']['title']['id'] = 'title';
        $this->handler->display->display_options['fields']['title']['table'] = 'node';
        $this->handler->display->display_options['fields']['title']['field'] = 'title';

        // Filter criterion: Content Type
        $this->handler->display->display_options['filters']['type']['id'] = 'type';
        $this->handler->display->display_options['filters']['type']['table'] = 'node';
        $this->handler->display->display_options['filters']['type']['field'] = 'type';
        $this->handler->display->display_options['filters']['type']['value'] = array($this->nodeType => $this->nodeType);

        // Add annotated class fields
        if($loadFields == true) {
            foreach($this->fields as $classVar => $fieldAnnotation) {
                $fieldName = $fieldAnnotation->name;
                $this->handler->display->display_options['fields'][$fieldName]['id'] = $fieldName;
                $this->handler->display->display_options['fields'][$fieldName]['table'] = 'field_data_'. $fieldName;
                $this->handler->display->display_options['fields'][$fieldName]['field'] = $fieldName;
            }
        }
    }

    public function fetch($nid, $nodeType, $field) {
        $this->relationshipField = $field;
        $this->relationshipClass = $nodeType;

        // Query by parent node.
        $this->handler->display->display_options['filters']['nid']['id'] = 'nid';
        $this->handler->display->display_options['filters']['nid']['table'] = 'node';
        $this->handler->display->display_options['filters']['nid']['field'] = 'nid';
        $this->handler->display->display_options['filters']['nid']['value']['value'] = $nid;

        // Setup Relationship
        $this->handler->display->display_options['relationships'][$this->relationshipField .'_nid']['id'] = $this->relationshipField .'_nid';
        $this->handler->display->display_options['relationships'][$this->relationshipField .'_nid']['table'] = 'field_data_'. $this->relationshipField;
        $this->handler->display->display_options['relationships'][$this->relationshipField .'_nid']['field'] = $this->relationshipField .'_nid';
        $this->handler->display->display_options['relationships'][$this->relationshipField .'_nid']['required'] = 0;
        $this->handler->display->display_options['relationships'][$this->relationshipField .'_nid']['delta'] = '-1';

        // Filter criterion: Content Type
        $this->handler->display->display_options['filters'][$this->relationshipField .'_type']['id'] = $this->relationshipField. '_type';
        $this->handler->display->display_options['filters'][$this->relationshipField. '_type']['table'] = 'node';
        $this->handler->display->display_options['filters'][$this->relationshipField. '_type']['field'] = 'type';
        $this->handler->display->display_options['filters'][$this->relationshipField. '_type']['value'] = array($this->relationshipClass => $this->relationshipClass);
        $this->handler->display->display_options['filters'][$this->relationshipField. '_type']['relationship'] = $this->relationshipField .'_nid';

        // Field: Content: Title
        $this->handler->display->display_options['fields']['title_'.$this->fetchCount]['id'] = 'title_'.$this->fetchCount;
        $this->handler->display->display_options['fields']['title_'.$this->fetchCount]['table'] = 'node';
        $this->handler->display->display_options['fields']['title_'.$this->fetchCount]['field'] = 'title';
        $this->handler->display->display_options['fields']['title_'.$this->fetchCount]['relationship'] = $this->relationshipField .'_nid';
        $this->fetchCount++;

        $annoReader = \Gltech\Annotations\AnnoReader::getInstance();
        $this->relationshipFields = $annoReader->getFields(new $this->relationshipClass);

        // Add annotated class fields
        foreach($this->relationshipFields as $classVar => $fieldAnnotation) {
            if($fieldAnnotation->belongs_to == null && $fieldAnnotation->belongs_to_many == null) {
                $refFieldName = $fieldAnnotation->name;
                $fieldWithSuffix = $this->relationshipField.'_'.$this->fetchCount;
                $this->handler->display->display_options['fields'][$fieldWithSuffix]['id'] = $fieldWithSuffix;
                $this->handler->display->display_options['fields'][$fieldWithSuffix]['table'] = 'field_data_'. $refFieldName;
                $this->handler->display->display_options['fields'][$fieldWithSuffix]['field'] = $refFieldName;
                $this->handler->display->display_options['fields'][$fieldWithSuffix]['relationship'] = $this->relationshipField .'_nid';
                $this->fetchCount++;
            }
        }

        return $this;
    }

    public function filter_text($field) {
        $fieldName = $this->getFieldName($field);
        $isField = false;

        if(!empty($this->relationshipClass)) {
            $fieldName = $this->getRelationshipFieldName($field);
        }

        if($fieldName != $field) {
            $isField = true;
        }

        $queryFilter = new ViewQueryFilterText($this, $this->handler, $fieldName, $isField, $this->relationshipField);
        return $queryFilter;
    }

    public function filter_num($field) {
        $fieldName = $this->getFieldName($field);
        $isField = false;

        if(!empty($this->relationshipClass)) {
            $fieldName = $this->getRelationshipFieldName($field);
        }

        if($fieldName != $field) {
            $isField = true;
        }

        $queryFilter = new ViewQueryFilterNum($this, $this->handler, $fieldName, $isField, $this->relationshipField);
        return $queryFilter;
    }

    public function filter_list($field) {
        $fieldName = $this->getFieldName($field);
        $isField = false;

        if(!empty($this->relationshipClass)) {
            $fieldName = $this->getRelationshipFieldName($field);
        }

        if($fieldName != $field) {
            $isField = true;
        }

        $queryFilter = new ViewQueryFilterList($this, $this->handler, $fieldName, $isField, $this->relationshipField);
        return $queryFilter;
    }

    public function filter_ref($field) {
        $fieldName = $this->getFieldName($field);
        $suffix = "nid";

        foreach($this->fields as $classVar => $fieldAnnotation) {
            if($classVar == $fieldName) {
                if((isset($fieldAnnotation->has_one) && strcasecmp($fieldAnnotation->has_one, "User") == 0) ||
                    (isset($fieldAnnotation->has_many) && strcasecmp($fieldAnnotation->has_many, "User") == 0) ||
                    (isset($fieldAnnotation->belongs_to_many) && strcasecmp($fieldAnnotation->belongs_to_many, "User") == 0))
                {
                    $suffix = "uid";
                }
            }
        }

        $queryFilter = new ViewQueryFilterRef($this, $this->handler, $fieldName, true, $this->relationshipField, $suffix);
        return $queryFilter;
    }

    public function filter_ref_field($fieldName) {
        $suffix = "nid";

        foreach($this->fields as $classVar => $fieldAnnotation) {
            if($classVar == $fieldName) {
                if((isset($fieldAnnotation->has_one) && strcasecmp($fieldAnnotation->has_one, "User") == 0) ||
                    (isset($fieldAnnotation->has_many) && strcasecmp($fieldAnnotation->has_many, "User") == 0) ||
                    (isset($fieldAnnotation->belongs_to_many) && strcasecmp($fieldAnnotation->belongs_to_many, "User") == 0))
                {
                    $suffix = "uid";
                }
            }
        }

        $queryFilter = new ViewQueryFilterRef($this, $this->handler, $fieldName, true, $this->relationshipField, $suffix);
        return $queryFilter;
    }

    private function getFieldName($name) {
        foreach($this->fields as $classVar => $fieldAnnotation) {
            if($classVar == $name) {
                return $fieldAnnotation->name;
            }
        }
        return $name;
    }

    private function getRelationshipFieldName($name) {
        foreach($this->relationshipFields as $classVar => $fieldAnnotation) {
            if($classVar == $name) {
                return $fieldAnnotation->name;
            }
        }
        return $name;
    }

    public function order($field, $direction = 'DESC') {
        $fieldName = $this->getFieldName($field);
        $fieldWithSuffix = $fieldName;
        $table = 'node';

        if($fieldName != $field) {
            $fieldWithSuffix = $fieldName."_value";
            $table = 'field_data_'.$fieldName;
        }

        $this->handler->display->display_options['sorts'][$fieldWithSuffix]['id'] = $fieldWithSuffix;
        $this->handler->display->display_options['sorts'][$fieldWithSuffix]['table'] = $table;
        $this->handler->display->display_options['sorts'][$fieldWithSuffix]['field'] = $fieldWithSuffix;
        $this->handler->display->display_options['sorts'][$fieldWithSuffix]['order'] = $direction;

        return $this;
    }

    public function execute() {
        $nodes = array();

        $this->view->execute();

        foreach($this->view->result as $result) {
            if(isset($this->relationshipClass)) {
                $relationshipData = $result->_field_data['node_field_data_'. $this->relationshipField .'_nid']['entity'];
                $targetClass = new $this->relationshipClass;
                $nodes[] = $targetClass->populateClass($relationshipData);
            }
            else {
                $typeClass = new $this->nodeType();
                $nodes[] = $typeClass->populateClass($result->_field_data['nid']['entity']);
            }
        }

        return $nodes;
    }

}
?>
