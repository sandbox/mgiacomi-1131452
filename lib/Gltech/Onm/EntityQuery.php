<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Onm;

class EntityQuery extends \EntityFieldQuery {
    private $nodeType;

    public function __construct($nodeType) {
        $this->nodeType = $nodeType;

        if($this->nodeType == "User") {
            // In the future we can add support for users, and other things.
        }
        else {
            $this->entityCondition('entity_type', 'node');
            $this->propertyCondition('type', $this->nodeType);
        }
    }

    public function execute() {
        $nodes = array();
        $results = parent::execute();

        if(!empty($results)) {
            $entities = entity_load('node', array_keys($results['node']));

            foreach($entities as $entity) {
                $typeClass = new $this->nodeType();
                $nodes[] = $typeClass->populateClass($entity);
            }
        }

        return $nodes;
    }
}
?>