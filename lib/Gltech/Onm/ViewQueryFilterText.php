<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Onm;

class ViewQueryFilterText {
    private $query;
    private $handler;
    private $fieldName;
    private $isField;
    private $relationshipField;

    public function __construct(&$query, &$handler, $fieldName, $isField, $relationshipField) {
        $this->query = $query;
        $this->handler = $handler;
        $this->fieldName = $fieldName;
        $this->isField = $isField;
        $this->relationshipField = $relationshipField;
    }

    public function equals($value) {
        $this->buildHandler($value);
        return $this->query;
    }

    public function not_equals($value) {
        $this->buildHandler($value, '!=');
        return $this->query;
    }

    public function contains($value) {
        $this->buildHandler($value, 'contains');
        return $this->query;
    }

    public function does_not_contain($value) {
        $this->buildHandler($value, 'not');
        return $this->query;
    }

    public function any_word($value) {
        $this->buildHandler($value, 'word');
        return $this->query;
    }

    public function all_words($value) {
        $this->buildHandler($value, 'allwords');
        return $this->query;
    }

    public function starts_with($value) {
        $this->buildHandler($value, 'starts');
        return $this->query;
    }

    public function not_starts_with($value) {
        $this->buildHandler($value, 'not_starts');
        return $this->query;
    }

    public function ends_with($value) {
        $this->buildHandler($value, 'ends');
        return $this->query;
    }

    public function not_ends_with($value) {
        $this->buildHandler($value, 'not_ends');
        return $this->query;
    }

    public function shorter_than($value) {
        $this->buildHandler($value, 'shorterthan');
        return $this->query;
    }

    public function longer_than($value) {
        $this->buildHandler($value, 'longerthan');
        return $this->query;
    }

    public function is_empty($value) {
        $this->buildHandler($value, 'empty');
        return $this->query;
    }

    public function not_empty($value) {
        $this->buildHandler($value, 'not_empty');
        return $this->query;
    }

    private function buildHandler($value, $op = null) {
        $fieldWithSuffix = $this->fieldName;
        $table = 'node';

        if($this->isField == true) {
            $fieldWithSuffix = $this->fieldName."_value";
            $table = 'field_data_'.$this->fieldName;
        }

        $this->handler->display->display_options['filters'][$fieldWithSuffix]['id'] = $fieldWithSuffix;
        $this->handler->display->display_options['filters'][$fieldWithSuffix]['table'] = $table;
        $this->handler->display->display_options['filters'][$fieldWithSuffix]['field'] = $fieldWithSuffix;

        if(isset($op)) {
            $this->handler->display->display_options['filters'][$fieldWithSuffix]['operator'] = $op;
        }

        $this->handler->display->display_options['filters'][$fieldWithSuffix]['value'] = $value;

        if(isset($this->relationshipField)) {
            $this->handler->display->display_options['filters'][$fieldWithSuffix]['relationship'] = $this->relationshipField .'_nid';
        }
    }

}
?>
