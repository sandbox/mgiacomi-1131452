<?php

namespace Gltech\Onm;

class ReferenceArray extends \ArrayObject {
    public function add($node) {
        $toAdd = $node;

        if(is_numeric($node)) {
            $toAdd = new static();
            $toAdd->nid = $node;
        }

        $this->append($toAdd);
    }
    public function remove($node) {
        $toRemove = $node;

        if(is_numeric($node)) {
            $toRemove = (object) array('nid' => $node);
        }

        $nodes = $this->getArrayCopy();

        foreach($nodes as $key => $val) {
            if($val->nid == $toRemove->nid) {
                unset($nodes[$key]);
            }
        }

        $this->exchangeArray($nodes);
    }
    public function set($nodes) {
        $newNodes = array();

        foreach($nodes as $node) {
            if(is_numeric($node)) {
                $toAdd = (object) array('nid' => $node);
                $newNodes[] = $toAdd;
            }
            else {
                $newNodes[] = $node;
            }
        }

        $this->exchangeArray($newNodes);
    }
    public function clear() {
        $this->exchangeArray(array());
    }
}

?>