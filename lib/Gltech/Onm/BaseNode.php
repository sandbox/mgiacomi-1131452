<?php

namespace Gltech\Onm;

class BaseNode {
    protected $vid;
    protected $uid;
    protected $title;
    protected $log;
    protected $status;
    protected $promote;
    protected $sticky;
    protected $comment;
    protected $nid;
    protected $type;
    protected $language;
    protected $created;
    protected $changed;
    protected $tnid;
    protected $translate;
    protected $revision_timestamp;
    protected $revision_uid;
    protected $cid;
    protected $last_comment_timestamp;
    protected $last_comment_name;
    protected $last_comment_uid;
    protected $comment_count;
    protected $name;
    protected $picture;
    protected $body;
    protected $references;

    public function __set($name, $value) {
        if(empty($this->{$name}) && is_array($this->references) &&
            array_key_exists($name, $this->references) &&
            $this->references[$name]->fetched == false)
        {
            $this->references[$name]->fetched = true;
        }

        if(!isset($value)) {
            $value = "";
        }
        $this->{$name} = $value;
    }

    public function __get($name) {
        if(empty($this->{$name}) && is_array($this->references) &&
           array_key_exists($name, $this->references) &&
           $this->references[$name]->fetched == false)
        {
            if($this->references[$name]->type == 'belongs_to') {
                $query = $this->getQueryByField($name);
                $results = $query->execute();
                if(count($results) > 1) {
                    throw new Exceptions\ConstraintViolation("Found multiple entries for a belongs_to for: ". $name);
                }
                $this->{$name} = $results[0];
                $this->references[$name]->fetched = true;
            }
            if($this->references[$name]->type == 'has_many' || $this->references[$name]->type == 'belongs_to_many') {
                $query = $this->getQueryByField($name);
                $results = $query->execute();
                $this->{$name} = new ReferenceArray($results);
                $this->references[$name]->fetched = true;
            }
            if($this->references[$name]->type == 'has_one') {
                if(isset($this->references[$name]->nid)) {
                    $hasOneClass = $this->references[$name]->nodeType;
                    $this->{$name} = $hasOneClass::find($this->references[$name]->nid);
                    $this->references[$name]->fetched = true;
                }
            }
        }

        return $this->{$name};
    }

    private function getQueryByField($name) {
        if(empty($data) && array_key_exists($name, $this->references)) {
            if($this->references[$name]->type == 'belongs_to' || $this->references[$name]->type == 'belongs_to_many') {
                $query = new ViewQuery(new $this->references[$name]->nodeType);
                $query->filter_ref_field($this->references[$name]->field)->contains($this->nid);
                return $query;
            }
            if($this->references[$name]->type == 'has_many') {
                $query = new ViewQuery($this, false);
                $query->fetch($this->nid, $this->references[$name]->nodeType, $this->references[$name]->field);
                return $query;
            }
        }

        throw new Exceptions\QueryException("Field ". $name ." can not be queried.");
    }

    public static function query() {
        return new ViewQuery(new static());
    }

    public function subQuery($field) {
        return $this->getQueryByField($field);
    }

    public static function entityQuery() {
        return new EntityQuery(get_class(new static()));
    }

    public static function create($title) {
        //global $language;
        $node = new static();
        $node->type = get_class($node);
        $node->language = 'und';
        $node->title = $title;
        return $node;
    }

    public function save() {
        $saveNode = new SaveNode();
        return $saveNode->save($this);
    }

    public static function find($nid) {
        $dnode = node_load($nid);
        if($dnode == false) {
            throw new Exceptions\NodeNotFound("No node found for nid ". $nid);
        }

        return self::populateClass($dnode);
    }

    public static function delete($node) {
        $type = new static();
        if(is_numeric($node)) {
            node_delete($node);
            return;
        }

        if(get_class($node) == get_class($type)) {
            if(isset($node->nid) && is_numeric($node->nid)) {
                node_delete($node->nid);
            }
            return;
        }
    }

    public static function populateClass($dnode) {
        $node = new static();
        $node->vid = $dnode->vid;
        $node->uid = $dnode->uid;
        $node->title = $dnode->title;
        $node->log = $dnode->log;
        $node->status = $dnode->status;
        $node->comment = $dnode->comment;
        $node->promote = $dnode->promote;
        $node->sticky = $dnode->sticky;
        $node->nid = $dnode->nid;
        $node->type = $dnode->type;
        $node->language = $dnode->language;
        $node->created = $dnode->created;
        $node->changed = $dnode->changed;
        $node->tnid = $dnode->tnid;
        $node->translate = $dnode->translate;
        $node->revision_timestamp = $dnode->revision_timestamp;
        $node->revision_uid = $dnode->revision_uid;
        $node->cid = $dnode->cid;
        $node->last_comment_timestamp = $dnode->last_comment_timestamp;
        $node->last_comment_name = $dnode->last_comment_name;
        $node->last_comment_uid = $dnode->last_comment_uid;
        $node->comment_count = $dnode->comment_count;
        $node->name = $dnode->name;
        $node->picture = $dnode->picture;

        if(isset($dnode->body[$dnode->language][0]['value'])) {
            $node->body = $dnode->body[$dnode->language][0]['value'];
        }

        $annoReader = \Gltech\Annotations\AnnoReader::getInstance();
        $fields = $annoReader->getFields($node);

        foreach($fields as $classVar => $fieldAnnotation) {
            $fieldName = $fieldAnnotation->name;

            if(isset($fieldName))  {
                $languages = field_available_languages('node', $fieldName);
                $field_language = LANGUAGE_NONE;

                if(in_array($dnode->language, $languages)) {
                    $field_language = $dnode->language;
                }

                if(!isset($fieldAnnotation->belongs_to) && !isset($fieldAnnotation->belongs_to_many)) {
                    $fieldData = $dnode->$fieldName;
                }

                if(isset($fieldAnnotation->belongs_to)) {
                    $node->references[$classVar] = new Reference($fieldAnnotation->belongs_to, $fieldName, $classVar, 'belongs_to');
                }
                else if(isset($fieldAnnotation->belongs_to_many)) {
                    $node->references[$classVar] = new Reference($fieldAnnotation->belongs_to_many, $fieldName, $classVar, 'belongs_to_many');
                }
                else if(isset($fieldAnnotation->has_many)) {
                    $node->references[$classVar] = new Reference($fieldAnnotation->has_many, $fieldName, $classVar, 'has_many');
                }
                else if(isset($fieldAnnotation->has_one)) {
                    $valueHolder = 'nid';
                    if(strcasecmp($fieldAnnotation->has_one, "User") == 0) {
                        $valueHolder = 'uid';
                    }
                    $value = null;
                    if(!empty($fieldData)) {
                        $value = $fieldData[$field_language][0][$valueHolder];
                    }
                    $reference = new Reference($fieldAnnotation->has_one, $fieldName, $classVar, 'has_one', $value);
                    $node->references[$classVar] = $reference;
                }
                else if(!empty($fieldData)) {
                    if(count($fieldData[$field_language]) > 1 || $fieldAnnotation->type == 'array') {
                        $values = array();
                        foreach($fieldData[$field_language] as $value) {
                            if(array_key_exists('value', $value)) {
                                $values[] = $value['value'];
                            }
                            else {
                                $values[] = $value;
                            }
                        }
                        $node->$classVar = $values;
                    }
                    else {
                        $node->$classVar = $fieldData[$field_language][0]['value'];
                    }
                }
            }
        }

        if(method_exists ($node, 'postPopulate')) {
            $node->postPopulate();
        }

        return $node;
    }
}
?>