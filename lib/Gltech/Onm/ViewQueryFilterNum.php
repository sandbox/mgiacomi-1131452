<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Onm;

class ViewQueryFilterNum {
    private $query;
    private $handler;
    private $fieldName;
    private $isField;
    private $relationshipField;

    public function __construct(&$query, &$handler, $fieldName, $isField, $relationshipField) {
        $this->query = $query;
        $this->handler = $handler;
        $this->fieldName = $fieldName;
        $this->isField = $isField;
        $this->relationshipField = $relationshipField;
    }

    public function equals($value) {
        $this->buildHandler($value);
        return $this->query;
    }

    public function not_equals($value) {
        $this->buildHandler($value, '!=');
        return $this->query;
    }

    public function less_than($value) {
        $this->buildHandler($value, '<');
        return $this->query;
    }

    public function less_than_equals($value) {
        $this->buildHandler($value, '<=');
        return $this->query;
    }

    public function greater_than($value) {
        $this->buildHandler($value, '>');
        return $this->query;
    }

    public function greater_than_equals($value) {
        $this->buildHandler($value, '>=');
        return $this->query;
    }

    public function between($value, $value2) {
        $this->buildHandler($value, 'between', $value2);
        return $this->query;
    }

    public function not_between($value, $value2) {
        $this->buildHandler($value, 'not between', $value2);
        return $this->query;
    }

    public function is_empty($value) {
        $this->buildHandler($value, 'empty');
        return $this->query;
    }

    public function not_empty($value) {
        $this->buildHandler($value, 'not_empty');
        return $this->query;
    }

    private function buildHandler($value, $op = null, $value2 = null) {
        $fieldWithSuffix = $this->fieldName;
        $table = 'node';

        if($this->isField == true) {
            $fieldWithSuffix = $this->fieldName."_value";
            $table = 'field_data_'.$this->fieldName;
        }

        $this->handler->display->display_options['filters'][$fieldWithSuffix]['id'] = $fieldWithSuffix;
        $this->handler->display->display_options['filters'][$fieldWithSuffix]['table'] = $table;
        $this->handler->display->display_options['filters'][$fieldWithSuffix]['field'] = $fieldWithSuffix;

        if(isset($op)) {
            $this->handler->display->display_options['filters'][$fieldWithSuffix]['operator'] = $op;
        }

        if(isset($op) && ($op == "between" || $op == "not between")) {
            $this->handler->display->display_options['filters'][$fieldWithSuffix]['value']['min'] = $value;
            $this->handler->display->display_options['filters'][$fieldWithSuffix]['value']['max'] = $value2;
        }
        else {
            $this->handler->display->display_options['filters'][$fieldWithSuffix]['value']['value'] = $value;
        }

        if(isset($this->relationshipField)) {
            $this->handler->display->display_options['filters'][$fieldWithSuffix]['relationship'] = $this->relationshipField .'_nid';
        }
    }

}
?>
