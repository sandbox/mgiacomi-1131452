<?php

namespace Gltech\Onm;

class SaveNode {
    public function save($node) {
        global $language;
        $default_language = $language->language;

        // If the user is not assigned then assign to current user.
        if(empty($node->name)) {
            $node->name = $GLOBALS['user']->name;
        }

        $dnode = array('type' => get_class($node), 'language' => $default_language);

        if($node->nid > 0) {
            $dnode = node_load($node->nid);
        }

        module_load_include('inc', 'node', 'node.pages');

        //$values = $this->createValues($node);
        $values = array();
        $values['values']['title'] = $node->title;
        $values['values']['status'] = $node->status;
        $values['values']['promote'] = $node->promote;
        $values['values']['sticky'] = $node->sticky;
        $values['values']['name'] = $node->name;
        $values['values']['op'] = t('Save');

        // Try to get the right body language
        $languages = field_available_languages('node', 'body');
        $body_language = LANGUAGE_NONE;

        if(in_array($default_language, $languages)) {
            $body_language = $default_language;
        }

        $values['values']['body'][$body_language][0]['value'] = $node->body;

        // Read annotations to determine fields to update
        $annoReader = \Gltech\Annotations\AnnoReader::getInstance();
        $fields = $annoReader->getFields($node);

        // We are only loading this node so we can determine how many references we need to pad by
        // in the case where we are removing a reference (or two)
        $theNodeThatShouldntBe = node_load($node->nid);

        foreach($fields as $classVar => $fieldAnnotation) {
            $languages = field_available_languages('node', $fieldAnnotation->name);
            $field_language = LANGUAGE_NONE;

            if(in_array($default_language, $languages)) {
                $field_language = $default_language;
            }

            if(isset($fieldAnnotation->has_one)) {
                if($node->$classVar != null && $node->$classVar->nid != null) {
                    $hasOneValue = '[nid:'. $node->$classVar->nid .']';
                    $values['values'][$fieldAnnotation->name][$field_language][0] = array('nid' => $hasOneValue);
                }
                else {
                    $values['values'][$fieldAnnotation->name][$field_language][0] = array('nid' => '');
                }
            }
            else if(isset($fieldAnnotation->has_many)) {
                if($node->$classVar instanceof \ArrayObject) {
                    $values['values'][$fieldAnnotation->name][$field_language] = array();
                    foreach($node->$classVar as $reference) {
                        if($reference->nid != null) {
                            $hasManyValue = '[nid:'. $reference->nid .']';
                            $values['values'][$fieldAnnotation->name][$field_language][] = array('nid' => $hasManyValue);
                        }
                    }

                    // Pad lists that are smaller then current saved values
                    if(count($theNodeThatShouldntBe->{$fieldAnnotation->name}) > 0) {
                        $origFieldArray = $theNodeThatShouldntBe->{$fieldAnnotation->name}['und'];

                        for ($i = count($node->$classVar); $i < count($origFieldArray); $i++) {
                            $values['values'][$fieldAnnotation->name][$field_language][] = array('nid' => '');
                        }
                    }
                }

                if(count($node->$classVar) == 0) {
                    $values['values'][$fieldAnnotation->name][$field_language][0] = array('nid' => '');
                }
            }
            else {
                $values['values'][$fieldAnnotation->name][$field_language][0]['value'] = $node->$classVar;
            }
        }
        return $this->submitForm($dnode, $node, $values);
    }

    private function submitForm($dnode, $node, $values) {
        $dnodeObject = (object)$dnode;

        // Get the current session messages before form submit
        $messages = null;
        if(isset($_SESSION['messages'])) {
            $messages = $_SESSION['messages'];
        }
        $this->drupal_form_submit_override($node, $values, $dnodeObject);

        // Reset session messages to their pre form submit state.
        $_SESSION['messages'] = $messages;

        // Deal with errors from the form submission
        $form_errors = form_get_errors();
        if(isset($form_errors)) {
            $errors = array();

            foreach($form_errors as $key=>$value) {
                $parts = preg_split('/\]\[/', $key);
                $error = array();
                $error['field'] = $parts[0];
                $error['language'] = $parts[1];
                $error['index'] = $parts[2];
                $error['error'] = strip_tags($value);
                $errors[] = $error;
            }

            $node->save_errors = $errors;
            return false;
            // Leave this as we may offer a setting to allow the to specify that validation errors are thrown.
            //throw new \Gltech\Onm\Exceptions\ValidationException($errors, "Failed Form Validation");
        }

        return true;
    }

    private function drupal_form_submit_override($srcnode, &$form_state, $node) {
        $form_id = get_class($srcnode) .'_node_form';
        if (!isset($form_state['build_info']['args'])) {
            $form_state['build_info']['args'] = array($node);
        }
        // Merge in default values.
        $form_state += form_state_defaults();

        // Populate $form_state['input'] with the submitted values before retrieving
        // the form, to be consistent with what drupal_build_form() does for
        // non-programmatic submissions (form builder functions may expect it to be
        // there).
        $form_state['input'] = $form_state['values'];

        $form_state['programmed'] = TRUE;
        $form = drupal_retrieve_form($form_id, $form_state);
        // Programmed forms are always submitted.

        //----------------------------------------
        // Read annotations to determine fields to update
        $annoReader = \Gltech\Annotations\AnnoReader::getInstance();
        $fields = $annoReader->getFields($srcnode);

        foreach($fields as $classVar => $fieldAnnotation) {
            if(isset($fieldAnnotation->has_many)) {
                // Go one level up in the form, to the widgets container.
                $element = drupal_array_get_nested_value($form, array($fieldAnnotation->name,'und'));
                $field_name = $element['#field_name'];
                $langcode = $element['#language'];
                $parents = $element['#field_parents'];

                // Increment the items count.
                $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);
                $field_state['items_count'] = count($srcnode->$classVar);
                field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);
            }
        }

        $form = drupal_rebuild_form($form_id, $form_state, $form);
        //----------------------------------------

        // Reset form validation.
        $form_state['submitted'] = TRUE;
        $form_state['must_validate'] = TRUE;
        form_clear_error();

        drupal_prepare_form($form_id, $form, $form_state);
        drupal_process_form($form_id, $form, $form_state);
    }
}
?>