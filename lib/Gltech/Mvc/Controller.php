<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Mvc;

class Controller {
    public function doAction($module, $prefix, $controller, $method) {
        $context = \Gltech\Context::getInstance();
        $context->mvcRoot = drupal_get_path('module', $module);
        $controllerClassPath = $context->mvcRoot .'/controllers/'. $controller .'.php';

        // Register Nodes for use in controllers and services
        $nodeLoader = new \Doctrine\Common\ClassLoader(null, $context->mvcRoot .'/nodes');
        $nodeLoader->register();

        if(file_exists($controllerClassPath)) {
            if(empty($method)) {
                $method = "index";
            }

            require_once $controllerClassPath;
            $controllerClass = new $controller();

            $requestMapper = new RequestMapper();
            $methodOverride = $requestMapper->methodOverride($controllerClass, $method);
            if(isset($methodOverride)) {
                $method = $methodOverride;
            }
            $context->module = $module;
            $context->prefix = $prefix;
            $context->controllerClass = $controllerClass;
            $context->controller = $controller;
            $context->action = $method;

            $securityMgr = new SecurityMgr();
            if(!$securityMgr->isMethodAuthorized($context->controllerClass, $method)) {
                throw new Exceptions\AccessDenied("Roll or Permission not found.");
            }

            if(method_exists($context->controllerClass, $method)) {
                $di = new \Gltech\Ioc\DependencyInjector();
                $di->injectDependencies($context->controllerClass);
                $proxyClass = $di->generateProxy($context->controllerClass);
                $request = new Request();
                $context->id = $request->id;
                return $proxyClass->$method($request);
            }
            else {
                throw new Exceptions\PageNotFound("Method not found: ". $controllerClassPath .' -> '. $method);
            }
        }
        else {
            throw new Exceptions\PageNotFound("Controller class not found: ". $controllerClassPath);
        }
    }

    public function getActionMethod() {
        return $this->actionMethod;
    }

    public function getControllerClass() {
        return $this->controllerClass;
    }

    public function getMvcRoot() {
        return $this->mvcRoot;
    }
}
