<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Mvc;

class RequestMapper {
    private $annoReader;

    public function __construct() {
        $this->annoReader = \Gltech\Annotations\AnnoReader::getInstance();
    }

    public function methodOverride($controllerClass, $map) {
        $requestMethod = $_SERVER['REQUEST_METHOD'];
        $requests = $this->annoReader->getRequests($controllerClass, $map);

        $methodOverride = null;
        foreach($requests as $methodName => $fieldAnnotation) {
            if($map == $fieldAnnotation->map) {
                $mapMethods = explode(",", $fieldAnnotation->method);
                foreach ($mapMethods as $mapMethod) {
                    if(strtolower($mapMethod) == strtolower($requestMethod)) {
                        if(isset($methodOverride)) {
                            $error = "Method ". $requestMethod. " has been defined for the following class methods ".
                                      get_class($controllerClass) .".". $methodOverride ."() and ".
                                      get_class($controllerClass) .".". $methodName ."()";

                            throw new Exceptions\RequestMapperException($error);
                        }
                        $methodOverride = $methodName;
                    }
                }
            }
        }

        return $methodOverride;
    }
}
