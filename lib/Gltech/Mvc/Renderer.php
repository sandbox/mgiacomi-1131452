<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Mvc;

class Renderer {
    private $context;

    public function __construct() {
        $this->context = \Gltech\Context::getInstance();
    }

    public function output($menu, $results) {
        if($results instanceof \Gltech\Mvc\Message) {
            if($results->getMessage() != null) {
                drupal_set_message($results->getMessage(), $results->getMessageType());
            }

            $this->setPageTitle($results);
        }

        if($results instanceof \Gltech\Mvc\Redirect) {
            drupal_goto($this->context->prefix .'/'. $results->controller .'/'. $results->action);
        }
        else if($results instanceof \Gltech\Mvc\Json) {
            drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
            print $results->encodeModel();
        }
        else {
            $menu->build();

            if($results instanceof \Gltech\Mvc\View) {
                $output = $this->processView($results->view, $results->model, $results->getCrumbs());
            }
            else {
                $output = $results;
            }

            return $output;
        }
    }

    private function processView($view, $model, $crumbs) {
        if(!isset($view)) {
            $view = $this->context->action;
        }

        if(!isset($model)) {
            $model = array();
        }

        $path = $this->context->mvcRoot .'/views/'. $this->context->controller .'/'. $view .'.tpl.php';

        if (file_exists($path) == false) {
            trigger_error("Template {$view}.tpl.php not found in ". $this->context->mvcRoot . '/views/'. $this->context->controller);
            return false;
        }

        $this->processBreadCrumbs($crumbs);

        if(is_array($model)) {
            // Load variables here because they become part of the module not the theme template.php file.
            foreach ($model as $key => $value) {
                $$key = $value;
            }

            // pre-load the child template output with variables
            $hook = $this->context->module .'__'. $this->context->controller .'__'. $this->context->action;
            return theme($hook, $model);
        }
    }

    private function setPageTitle($results) {
        if($results->getTitle() != null) {
            drupal_set_title($results->getTitle());
        }
        else {
            $annoReader = \Gltech\Annotations\AnnoReader::getInstance();
            $page = $annoReader->getPage($this->context->controllerClass, $this->context->action);
            if(isset($page->title)) {
                drupal_set_title($page->title);
            }
        }
    }

    private function processBreadCrumbs($crumbs) {
        $annoReader = \Gltech\Annotations\AnnoReader::getInstance();
        $controllerCrumb = $annoReader->getControllerCrumbs($this->context->controllerClass);
        $actionCrumb = $annoReader->getActionCrumbs($this->context->controllerClass, $this->context->action);
        $moduleContext = $this->context->modconfig['context'];

        if(isset($this->context->modconfig['title'])) {
            $moduleTitle = $this->context->modconfig['title'];
        }

        if(isset($this->context->modconfig['default_controller'])) {
            $moduleDefaultController = $this->context->modconfig['default_controller'];
        }

        if(isset($this->context->modconfig['default_action'])) {
            $moduleDefaultAction = $this->context->modconfig['default_action'];
        }

        $breadcrumbs = array();
        $breadcrumbs[] = l(t('Home'), NULL);

        $addControllerCrumb = null;
        if(isset($controllerCrumb->title) && !empty($controllerCrumb->title)) {
            if(isset($moduleContext)) {
                $url = $moduleContext;

                if(isset($controllerCrumb->controller)) {
                    $url .= '/'. $controllerCrumb->controller;
                }
                else {
                    $url .= '/'. $this->context->controller;
                }

                if(isset($controllerCrumb->action)) {
                    $url .= '/'. $controllerCrumb->action;
                }

                $addControllerCrumb = l(t($controllerCrumb->title), $url);
            }
            else {
                $addControllerCrumb = t($controllerCrumb->title);
            }
        }

        $addActionCrumb = null;
        if(isset($actionCrumb->title) && !empty($actionCrumb->title)) {
            if(isset($moduleContext) && (isset($actionCrumb->controller) || isset($actionCrumb->action))) {
                $url = $moduleContext;

                if(isset($actionCrumb->controller)) {
                    $url .= '/'. $actionCrumb->controller;
                }
                else {
                    $url .= '/'. $this->context->controller;
                }

                if(isset($actionCrumb->action)) {
                    $url .= '/'. $actionCrumb->action;
                }

                $addActionCrumb = l(t($actionCrumb->title), $url);
            }
            else {
                $addActionCrumb = t($actionCrumb->title);
            }
        }

        if(isset($moduleTitle)) {
            $title = drupal_get_title();
            if(isset($moduleContext) && isset($moduleDefaultController) && isset($moduleDefaultAction) &&
               (isset($addActionCrumb) || isset($addControllerCrumb) || !empty($title) || count($crumbs) > 0))
            {
                if(isset($addControllerCrumb)) {
                    $breadcrumbs[] = $addControllerCrumb;
                }
                if(isset($addActionCrumb)) {
                    $breadcrumbs[] = $addActionCrumb;
                }

                $breadcrumbs[] = l(t($moduleTitle), $moduleContext);

                if(count($crumbs) > 0) {
                    $breadcrumbs = array_merge($breadcrumbs, $crumbs);
                }
            }
            else {
                $breadcrumbs[] = t($moduleTitle);
            }
        }

        if(count($breadcrumbs) > 0) {
            drupal_set_breadcrumb($breadcrumbs);
        }
    }
}