<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Mvc;
 
class AuthHelper {
    private $securityMgr;

    public function __construct() {
        $this->securityMgr = new SecurityMgr();
    }

    public function hasRole($roles) {
        $secured = new \Gltech\Annotations\Secured(array());
        $secured->roles = $roles;
        return $this->securityMgr->isAuthorized($secured);
    }

    public function hasPermission($permissions) {
        $secured = new \Gltech\Annotations\Secured(array());
        $secured->permissions = $permissions;
        return $this->securityMgr->isAuthorized($secured);
    }

    public function user($users) {
        $secured = new \Gltech\Annotations\Secured(array());
        $secured->users = $users;
        return $this->securityMgr->isAuthorized($secured);
    }

    public function authenticated() {
        $secured = new \Gltech\Annotations\Secured(array());
        $secured->authenticated = true;
        return $this->securityMgr->isAuthorized($secured);
    }

    public function anonymous() {
        $secured = new \Gltech\Annotations\Secured(array());
        $secured->anonymous = true;
        return $this->securityMgr->isAuthorized($secured);
    }
}
