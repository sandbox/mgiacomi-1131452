<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Mvc;


class NestedValueParser {
    private $lexer;
    protected $isNestedAnnotation = false;

    public function __construct() {
        $this->lexer = new \Doctrine\Common\Annotations\Lexer;
    }

    /**
     * Parses the given docblock string for annotations.
     *
     * @param string $docBlockString The docblock string to parse.
     * @return array Array of annotations. If no annotations are found, an empty array is returned.
     */
    public function parse($input) {
        $this->lexer->reset();
        $this->lexer->setInput($input);
        $this->lexer->moveNext();
        $this->lexer->resetPosition();
        return $this->Annotation();
    }

    /**
     * Attempts to match the given token with the current lookahead token.
     * If they match, updates the lookahead token; otherwise raises a syntax error.
     *
     * @param int Token type.
     * @return bool True if tokens match; false otherwise.
     */
    public function match($token) {
        if ( ! ($this->lexer->lookahead['type'] === $token)) {
            $this->syntaxError($this->lexer->getLiteral($token));
        }
        $this->lexer->moveNext();
    }

    /**
     * Generates a new syntax error.
     *
     * @param string $expected Expected string.
     * @param array $token Optional token.
     * @throws AnnotationException
     */
    private function syntaxError($expected, $token = null) {
        if ($token === null) {
            $token = $this->lexer->lookahead;
        }

        $message =  "Expected {$expected}, got ";

        if ($this->lexer->lookahead === null) {
            $message .= 'end of string';
        } else {
            $message .= "'{$token['value']}' at position {$token['position']}";
        }

        if (strlen($this->context)) {
            $message .= ' in ' . $this->context;
        }

        $message .= '.';

        throw \Doctrine\Common\Annotations\AnnotationException::syntaxError($message);
    }

    public function Annotation() {
        $values = array();
        $nameParts = array();

        if ($this->isNestedAnnotation === false) {

            if ($this->lexer->lookahead['type'] !== \Doctrine\Common\Annotations\Lexer::T_IDENTIFIER) {
                return false;
            }
            $this->lexer->moveNext();
        } else {
            $this->match(\Doctrine\Common\Annotations\Lexer::T_IDENTIFIER);
        }
        $nameParts[] = $this->lexer->token['value'];

        $this->isNestedAnnotation = true;
        $values = $this->Values();

        return $values;
    }

    /**
     * Values ::= Array | Value {"," Value}*
     *
     * @return array
     */
    public function Values() {
        $values = array();

        $values[] = $this->Value();

        while ($this->lexer->isNextToken(\Doctrine\Common\Annotations\Lexer::T_COMMA)) {
            $this->match(\Doctrine\Common\Annotations\Lexer::T_COMMA);
            $token = $this->lexer->lookahead;
            $value = $this->Value();

            if ( ! is_object($value) && ! is_array($value)) {
                $this->syntaxError('Value', $token);
            }

            $values[] = $value;
        }

        foreach ($values as $k => $value) {
            if (is_object($value) && $value instanceof \stdClass) {
                $values[$value->name] = $value->value;
            } else if ( ! isset($values['value'])){
                $values['value'] = $value;
            } else {
                if ( ! is_array($values['value'])) {
                    $values['value'] = array($values['value']);
                }

                $values['value'][] = $value;
            }

            unset($values[$k]);
        }

        return $values;
    }

    /**
     * Value ::= PlainValue | FieldAssignment
     *
     * @return mixed
     */
    public function Value() {
        $peek = $this->lexer->glimpse();

        if ($peek['value'] === '=') {
            return $this->FieldAssignment();
        }

        return $this->PlainValue();
    }

    /**
     * PlainValue ::= integer | string | float | boolean | Array | Annotation
     *
     * @return mixed
     */
    public function PlainValue() {
        switch ($this->lexer->lookahead['type']) {
            case \Doctrine\Common\Annotations\Lexer::T_STRING:
                $this->match(\Doctrine\Common\Annotations\Lexer::T_STRING);
                return $this->lexer->token['value'];

            case \Doctrine\Common\Annotations\Lexer::T_INTEGER:
                $this->match(\Doctrine\Common\Annotations\Lexer::T_INTEGER);
                return (int)$this->lexer->token['value'];

            case \Doctrine\Common\Annotations\Lexer::T_FLOAT:
                $this->match(\Doctrine\Common\Annotations\Lexer::T_FLOAT);
                return (float)$this->lexer->token['value'];

            case \Doctrine\Common\Annotations\Lexer::T_TRUE:
                $this->match(\Doctrine\Common\Annotations\Lexer::T_TRUE);
                return true;

            case \Doctrine\Common\Annotations\Lexer::T_FALSE:
                $this->match(\Doctrine\Common\Annotations\Lexer::T_FALSE);
                return false;

            default:
//                $this->syntaxError('PlainValue');
        }
    }

    /**
     * FieldAssignment ::= FieldName "=" PlainValue
     * FieldName ::= identifier
     *
     * @return array
     */
    public function FieldAssignment() {
        $this->match(\Doctrine\Common\Annotations\Lexer::T_IDENTIFIER);
        $fieldName = $this->lexer->token['value'];
        $this->match(\Doctrine\Common\Annotations\Lexer::T_EQUALS);

        $item = new \stdClass();
        $item->name  = $fieldName;
        $item->value = $this->PlainValue();

        return $item;
    }

    /**
     * Array ::= "{" ArrayEntry {"," ArrayEntry}* "}"
     *
     * @return array
     */
    public function Arrayx() {
        $array = $values = array();

        $this->match(\Doctrine\Common\Annotations\Lexer::T_OPEN_CURLY_BRACES);
        $values[] = $this->ArrayEntry();

        while ($this->lexer->isNextToken(\Doctrine\Common\Annotations\Lexer::T_COMMA)) {
            $this->match(\Doctrine\Common\Annotations\Lexer::T_COMMA);
            $values[] = $this->ArrayEntry();
        }

        $this->match(\Doctrine\Common\Annotations\Lexer::T_CLOSE_CURLY_BRACES);

        foreach ($values as $value) {
            list ($key, $val) = $value;

            if ($key !== null) {
                $array[$key] = $val;
            } else {
                $array[] = $val;
            }
        }

        return $array;
    }

    /**
     * ArrayEntry ::= Value | KeyValuePair
     * KeyValuePair ::= Key "=" PlainValue
     * Key ::= string | integer
     *
     * @return array
     */
    public function ArrayEntry() {
        $peek = $this->lexer->glimpse();

        if ($peek['value'] == '=') {
            $this->match(
                $this->lexer->isNextToken(\Doctrine\Common\Annotations\Lexer::T_INTEGER) ? \Doctrine\Common\Annotations\Lexer::T_INTEGER : \Doctrine\Common\Annotations\Lexer::T_STRING
            );

            $key = $this->lexer->token['value'];
            $this->match(\Doctrine\Common\Annotations\Lexer::T_EQUALS);

            return array($key, $this->PlainValue());
        }

        return array(null, $this->Value());
    }
}