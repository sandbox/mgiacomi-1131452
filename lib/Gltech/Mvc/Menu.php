<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Mvc;

class Menu {
    public $data;
    public $title;

    public function build() {
        $context = \Gltech\Context::getInstance();
        $annoReader = \Gltech\Annotations\AnnoReader::getInstance();
        $menuName = $annoReader->getMenu($context->controllerClass, $context->action);
        $menuFile = $context->mvcRoot .'/menus/'. $menuName .'.xml';

        if(isset($menuName)) {
            if(!file_exists($menuFile)) {
                if($menuName != 'default') {
                    trigger_error("Menu {$menuName}.xml not found in ". $context->mvcRoot . '/menus');
                }
                return;
            }

            $xml = simplexml_load_file($menuFile);
            $this->data = $this->simpleXMLToArray($xml, $context);
        }
    }

    private function simpleXMLToArray($xml, $context) {
        $return = array();
        if(!($xml instanceof \SimpleXMLElement)) { return $return; }
        $_value = trim((string)$xml);

        if($xml->getName() == "menu") {
            $attributes = $xml->attributes();
            $this->title = (String)$attributes['title'];
        }

        if(strlen($_value)==0) { $_value = null; };
        if($_value!==null) { $return = $_value; }

        $links = array();
        $counter = 0;

        foreach($xml->children() as $elementName => $child){
            $childArray = $this->simpleXMLToArray($child, $context);

            if($elementName == "link") {
                $attributes = $child->attributes();

                $securityMgr = new SecurityMgr();
                if($securityMgr->showMenu((String)$attributes['roles'], (String)$attributes['permissions'],
                                          (String)$attributes['users'], (String)$attributes['authenticated']))
                {
                    if(!empty($attributes['title']) && !empty($attributes['href'])) {
                        $link = array("link" => array(
                            "title" => (String)$attributes['title'],
                            "menu_name" => "glmenu",
                            "href" => (String)$attributes['href'],
                            "access" => true,
                            "hidden" => false,
                            "has_children" => 0,
                            "in_active_trail" => false,
                            "mlid" => $counter,
                        ));

                        if(!empty($attributes['active']) && $attributes['active'] == true) {
                            $link['link']['in_active_trail'] = true;
                        }

                        if(!empty($childArray)) {
                            $link['below'] = $childArray;
                            $link['link']['has_children'] = count($childArray);
                        }
                        else {
                            $link['below'] = array();
                        }

                        $links[$counter] = $link;
                    }

                    if(!empty($attributes['title']) && !empty($attributes['controller']) && !empty($attributes['action'])) {
                        $url = $context->prefix .'/'. $attributes['controller'] .'/'. $attributes['action'];

                        $link = array("link" => array(
                            "title" => (String)$attributes['title'],
                            "menu_name" => "glmenu",
                            "href" => $url,
                            "access" => true,
                            "hidden" => false,
                            "has_children" => 0,
                            "in_active_trail" => false,
                            "mlid" => $counter,
                            "below" => array()
                        ));

                        if($context->controller == $attributes['controller'] && $context->action == $attributes['action']) {
                            $link['link']['in_active_trail'] = true;
                        }

                        if(!empty($childArray)) {
                            $link['below'] = $childArray;
                            $link['link']['has_children'] = count($childArray);
                        }
                        else {
                            $link['below'] = array();
                        }


                        $links[$counter] = $link;
                    }
                }
            }

            $counter++;
        }

        return $links;
    }
}
