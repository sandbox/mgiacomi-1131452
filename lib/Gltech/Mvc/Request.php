<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Mvc;

class Request {
    private static $jsonDataCache;

    private static function getJsonParameter($parameter) {
        if(($_SERVER['REQUEST_METHOD'] == 'POST' || $_SERVER['REQUEST_METHOD'] == 'PUT') && $_SERVER["CONTENT_TYPE"] = 'application/json') {
            if(empty(self::$jsonDataCache)) {
                self::$jsonDataCache = json_decode(file_get_contents('php://input'));
            }

            if(isset(self::$jsonDataCache->$parameter)) {
                return self::$jsonDataCache->$parameter;
            }
        }
    }

    public static function getOffset() {
        $voom_context = \Gltech\Context::getInstance();
        if($voom_context->context_as_controller == true) {
            return 2;
        }

        return 3;
    }

    public function __get($key) {
        if($key == "id") {
            $stringZero = $this->getString(0);
            if(isset($stringZero)) {
                return $this->getString(0);
            }
        }

        return $this->getString($key);
    }

    public static function getString($parameter, $default = null) {
        if(is_numeric($parameter)) {
            return arg($parameter + Request::getOffset());
        }

        if(isset($_GET[$parameter])) {
            return $_GET[$parameter];
        }
        if(isset($_POST[$parameter])) {
            return $_POST[$parameter];
        }

        $value = Request::getJsonParameter($parameter);
        if(isset($value)) {
            return $value;
        }

        return $default;
    }

    public static function getDate($parameter, $default = null) {
        if(is_numeric($parameter)) {
            return \DateTime::createFromFormat('m#d#Y H:i:s', arg($parameter + 3)."0:0:0");
        }

        if(isset($_GET[$parameter])) {
            return \DateTime::createFromFormat('m#d#Y H:i:s', $_GET[$parameter]."0:0:0");
        }
        if(isset($_POST[$parameter])) {
            return \DateTime::createFromFormat('m#d#Y H:i:s', $_POST[$parameter]."0:0:0");
        }

        $value = Request::getJsonParameter($parameter);
        if(isset($value)) {
            return \DateTime::createFromFormat('m#d#Y H:i:s', $value."0:0:0");
        }


        return \DateTime::createFromFormat('m#d#Y H:i:s', $default."0:0:0");
    }

    public static function getTime($parameter, $default = null) {
        if(is_numeric($parameter)) {
            return \DateTime::createFromFormat('m#d#Y H:i:s', arg($parameter + 3));
        }

        if(isset($_GET[$parameter])) {
            return \DateTime::createFromFormat('m#d#Y H:i:s', $_GET[$parameter]);
        }
        if(isset($_POST[$parameter])) {
            return \DateTime::createFromFormat('m#d#Y H:i:s', $_POST[$parameter]);
        }

        $value = getJsonParameter($parameter);
        if(isset($value)) {
            return \DateTime::createFromFormat('m#d#Y H:i:s', $value);
        }

        return \DateTime::createFromFormat($default, 'm#d#Y H:i:s');
    }

    public static function getInt($parameter, $default = null) {
        if(is_numeric($parameter)) {
            return (int)arg($parameter + Request::getOffset());
        }

        if(isset($_GET[$parameter])) {
            return (int)$_GET[$parameter];
        }
        if(isset($_POST[$parameter])) {
            return (int)$_POST[$parameter];
        }

        $value = Request::getJsonParameter($parameter);
        if(isset($value)) {
            return (int)$value;
        }

        return (int)$default;
    }
}
