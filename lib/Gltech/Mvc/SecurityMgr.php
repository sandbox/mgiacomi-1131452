<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Mvc;

class SecurityMgr {
    private $annoReader;

    public function __construct() {
        $this->annoReader = \Gltech\Annotations\AnnoReader::getInstance();
    }

    public function isMember($role) {
        global $user;
        return (in_array($role, array_values($user->roles)));
    }

    public function isMethodAuthorized($controllerClass, $action) {
        $secured = $this->annoReader->getSecured($controllerClass, $action);
        return $this->isAuthorized($secured);
    }

    public function showMenu($roles, $permissions, $users, $authenticated) {
        $secured = new \Gltech\Annotations\Secured(array());
        $secured->roles = $roles;
        $secured->permissions = $permissions;
        $secured->users = $users;
        $secured->authenticated = $authenticated;

        return $this->isAuthorized($secured);
    }

    public function isModuleAuthorized($modconfig) {
        $secured = new \Gltech\Annotations\Secured(array());

        if(isset($modconfig['roles_allowed'])) {
            $secured->roles = $modconfig['roles_allowed'];
        }
        if(isset($modconfig['permissions_allowed'])) {
            $secured->permissions = $modconfig['permissions_allowed'];
        }
        if(isset($modconfig['users_allowed'])) {
            $secured->users = $modconfig['users_allowed'];
        }
        if(isset($modconfig['authenticated'])) {
            $secured->authenticated = $modconfig['authenticated'];
        }

        return $this->isAuthorized($secured);
    }

    public function isAuthorized($secured) {
        // Authorized if no annotation is present.
        if(empty($secured)) {
            return true;
        }
        if(empty($secured->roles) && empty($secured->permissions) && empty($secured->users) &&
           empty($secured->authenticated) && empty($secured->anonymous))
        {
            return true;
        }

        // Check Authorized
        if($secured->authenticated == 'true' && $this->isMember('anonymous user')) {
            return false;
        }
        if($secured->authenticated == 'true' && $this->isMember('anonymous user') == false) {
            return true;
        }
        if($secured->anonymous == 'true' && $this->isMember('anonymous user')) {
            return true;
        }

        global $user;
        // Check for User match
        if(!empty($secured->users)) {
            $users = explode(",", $secured->users);
            foreach ($users as $username) {
                if(isset($user->name) && $user->name == $username) {
                    return true;
                }
            }
        }

        // Check for Role match
        if(!empty($secured->roles)) {
            $roles = explode(",", $secured->roles);
            foreach ($roles as $role) {
                if($this->isMember($role)) {
                    return true;
                }
            }
        }

        // Check for Permission match
        if(!empty($secured->permissions)) {
            $permissions = explode(",", $secured->permissions);
            foreach ($permissions as $permission) {
                if(isset($permission) && $permission && user_access($permission)) {
                    return true;
                }
            }
        }

        return false;
    }
}
