<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Mvc;

class Message {
    private $message;
    private $messageType;
    private $title;
    private $crumbs = array();

    function getMessage() {
        return $this->message;
    }

    function getMessageType() {
        return $this->messageType;
    }

    function getTitle() {
        return $this->title;
    }

    function getCrumbs() {
        return $this->crumbs;
    }

    function status($message) {
        $this->message = $message;
        $this->messageType = "status";
        return $this;
    }
    function warning($message) {
        $this->message = $message;
        $this->messageType = "warning";
        return $this;
    }
    function error($message) {
        $this->message = $message;
        $this->messageType = "error";
        return $this;
    }
    function title($title) {
        $this->title = $title;
        return $this;
    }
    function crumbs($crumbs) {
        $this->crumbs = $crumbs;
        return $this;
    }

    function addCrumb($crumb) {
        $this->crumbs[] = $crumb;
        return $this;
    }
}
