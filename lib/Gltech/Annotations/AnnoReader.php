<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL.
 *
 * Author: Matt Giacomini <http://www.gltech.com>
 */

namespace Gltech\Annotations;

class AnnoReader {
    private static $m_pInstance;
    private $annotationReader;

    private function __construct() {
        $this->annotationReader = new \Doctrine\Common\Annotations\AnnotationReader();
        $this->annotationReader->setDefaultAnnotationNamespace('Gltech\Annotations\\');
        $this->annotationReader->setAutoloadAnnotations(true);
    }

    public static function getInstance() {
        if (!self::$m_pInstance) {
            self::$m_pInstance = new AnnoReader();
        }

        return self::$m_pInstance;
    }

    public function getControllerCrumbs($controller) {
        $class = new \ReflectionClass($controller);
        $classAnnotations = $this->annotationReader->getClassAnnotations($class);
        if(isset($classAnnotations['Gltech\Annotations\Breadcrumb'])) {
            return $classAnnotations['Gltech\Annotations\Breadcrumb'];
        }
    }

    public function getActionCrumbs($controller, $action) {
        $method = new \ReflectionMethod($controller, $action);
        $methodAnnotations = $this->annotationReader->getMethodAnnotations($method);
        if(isset($methodAnnotations['Gltech\Annotations\Breadcrumb'])) {
            return $methodAnnotations['Gltech\Annotations\Breadcrumb'];
        }
    }

    public function getPage($controller, $action) {
        $method = new \ReflectionMethod($controller, $action);
        $methodAnnotations = $this->annotationReader->getMethodAnnotations($method);
        if(isset($methodAnnotations['Gltech\Annotations\Page'])) {
            return $methodAnnotations['Gltech\Annotations\Page'];
        }

        $class = new \ReflectionClass($controller);
        $classAnnotations = $this->annotationReader->getClassAnnotations($class);
        if(isset($classAnnotations['Gltech\Annotations\Page'])) {
            return $classAnnotations['Gltech\Annotations\Page'];
        }
    }

    public function getMenu($controller, $action) {
        $method = new \ReflectionMethod($controller, $action);
        $methodAnnotations = $this->annotationReader->getMethodAnnotations($method);
        if(isset($methodAnnotations['Gltech\Annotations\Menu']->name)) {
            return $methodAnnotations['Gltech\Annotations\Menu']->name;
        }

        $class = new \ReflectionClass($controller);
        $classAnnotations = $this->annotationReader->getClassAnnotations($class);
        if(isset($classAnnotations['Gltech\Annotations\Menu']->name)) {
            return $classAnnotations['Gltech\Annotations\Menu']->name;
        }

        return "default";
    }

    public function getSecured($controller, $action) {
        $method = new \ReflectionMethod($controller, $action);
        $methodAnnotations = $this->annotationReader->getMethodAnnotations($method);
        if(isset($methodAnnotations['Gltech\Annotations\Secured'])) {
            return $methodAnnotations['Gltech\Annotations\Secured'];
        }

        $class = new \ReflectionClass($controller);
        $classAnnotations = $this->annotationReader->getClassAnnotations($class);
        if(isset($classAnnotations['Gltech\Annotations\Secured'])) {
            return $classAnnotations['Gltech\Annotations\Secured'];
        }
    }

    public function isTranactional($method) {
        $methodAnnotations = $this->annotationReader->getMethodAnnotations($method);
        if(isset($methodAnnotations['Gltech\Annotations\Transactional'])) {
            return true;
        }
        return false;
    }

    public function getInjectProperties($class) {
        $injectables = array();
        $reflect = new \ReflectionClass($class);
		$props = $reflect->getProperties();

        foreach($props as $prop) {
            $annotation = $this->annotationReader->getPropertyAnnotations($prop);
            if(array_key_exists('Gltech\Annotations\Inject', $annotation)) {
                if(!empty($annotation['Gltech\Annotations\Inject']->database)) {
                    $injectables[$prop->name] = array('type'=>'database', 'value'=> $annotation['Gltech\Annotations\Inject']->database);
                }
                else {
                    $injectables[$prop->name] = array('type'=>'class', 'value'=> $annotation['Gltech\Annotations\Inject']->class);
                }
            }
        }

        return $injectables;
    }

    public function getFields($class) {
        $fields = array();
        $reflect = new \ReflectionClass($class);
		$props = $reflect->getProperties();

        foreach($props as $prop) {
            $annotation = $this->annotationReader->getPropertyAnnotations($prop);
            if(array_key_exists('Gltech\Annotations\Field', $annotation)) {
                $fields[$prop->name] = $annotation['Gltech\Annotations\Field'];
            }
        }

        return $fields;
    }

    public function getRequests($class, $map) {
        $requests = array();
        $reflect = new \ReflectionClass($class);
		$methods = $reflect->getMethods();

        foreach($methods as $method) {
            $annotation = $this->annotationReader->getMethodAnnotations($method);
            if(array_key_exists('Gltech\Annotations\Request', $annotation)) {
                if($annotation['Gltech\Annotations\Request']) {
                    $requests[$method->name] = $annotation['Gltech\Annotations\Request'];
                }
            }
        }

        return $requests;
    }
}
